class ContactMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(contact, myparams=nil)
    @contact = contact
    @myparams = myparams if myparams.present?
    mail(to: "barb@educonsulting.info", subject: 'You have received a new contact from your website')
  end
end
