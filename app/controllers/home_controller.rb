class HomeController < ApplicationController
  def index
    @blogs = Blog.last(3)
  end

  def about
  end

  def college_consulting
  end

  def international_students
  end

  def additional_services
  end

  def post_additional_services
    safe_params = params.permit!
    myparams = safe_params.to_h
    @contact = Contact.create(email: params[:additional_services][:email], name: params[:additional_services][:child_name])
    ContactMailer.welcome_email(@contact, myparams).deliver_later
    redirect_to root_path
  end

  def post_international_students
    safe_params = params.permit!
    myparams = safe_params.to_h
    @contact = Contact.create(email: params[:international_students][:email], name: params[:international_students][:child_name])
    ContactMailer.welcome_email(@contact, myparams).deliver_later
    redirect_to root_path
  end

end
