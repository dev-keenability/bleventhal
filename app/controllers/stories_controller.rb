class StoriesController < ApplicationController
  before_action :set_story, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  def index
    @stories = Story.all
  end

  def show
  end

  def story_post
  end

  def new
    @story = Story.new
  end

  def edit
  end

  def create
    @story = Story.new(story_params)
    if @story.save
      redirect_to story_path(@story.title_for_slug), notice: 'Student Story was successfully created.'
    else
      render :new
    end
  end

  def update
    if @story.update(story_params)
      redirect_to story_path(@story.title_for_slug), notice: 'Student Story was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @story.destroy
    redirect_to stories_url, notice: 'Student Story was successfully destroyed.'
  end

  private

    def set_story
      @story = Story.find_by(title_for_slug: params[:title_for_slug])
    end

    def story_params
      params.require(:story).permit(:title, :author, :content, :content_index, :title_for_slug, :main_image, :pubdate, :meta_description, :meta_keywords, images_attributes: [:id, :pic, :name, :_destroy])
    end

end
