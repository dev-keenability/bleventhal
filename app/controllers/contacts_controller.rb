class ContactsController < ApplicationController
  def create
    @contact = Contact.new(contact_params)

    if @contact.save
      # Tell the contactMailer to send a welcome email after save
      ContactMailer.welcome_email(@contact).deliver_later
      flash[:success] = 'Thank you for contacting us.'
      redirect_to "/about"
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone_number, :child_grade_level, :message)
  end
end
