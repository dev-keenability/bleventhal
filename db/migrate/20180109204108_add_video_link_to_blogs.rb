class AddVideoLinkToBlogs < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :video_link, :string
  end
end
