class AddChildGradeLevelToContact < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :child_grade_level, :string
  end
end
