class CreateStories < ActiveRecord::Migration[5.0]
  def change
    create_table :stories do |t|
      t.string :title
      t.string :author
      t.text :content
      t.text :content_index
      t.string :title_for_slug
      t.string :main_image
      t.datetime :pubdate
      t.string :meta_description
      t.string :meta_keywords
      t.string :title_for_slug

      t.timestamps
    end
    add_index :stories, :title_for_slug
  end
end
