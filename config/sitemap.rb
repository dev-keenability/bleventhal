# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.educonsulting.info"

SitemapGenerator::Sitemap.adapter = SitemapGenerator::AwsSdkAdapter.new(
  "bleventhal",
  aws_access_key_id: ENV["S3_ACCESS_KEY_ID"],
  aws_secret_access_key: ENV["S3_SECRET_ACCESS_KEY"],
  aws_region: 'us-east-1'
)

SitemapGenerator::Sitemap.sitemaps_host = "https://s3.amazonaws.com/bleventhal/"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end


  add blogs_path, :priority => 0.8, :changefreq => 'weekly'
  Blog.find_each do |blog|
    add blog_path(blog.title_for_slug), :changefreq => 'weekly', :lastmod => blog.updated_at, :priority => 0.8
  end

  add about_path, :priority => 0.8, :changefreq => 'monthly'
  add college_consulting_path, :priority => 0.8, :changefreq => 'monthly'
  add international_students_path, :priority => 0.8, :changefreq => 'monthly'
  add additional_services_path, :priority => 0.8, :changefreq => 'monthly'
  # ...
end