Rails.application.routes.draw do
  require "admin_constraint"
  mount Ckeditor::Engine => "/ckeditor", constraints: AdminConstraint.new

  #########################################
  #==news url for blog model
  #########################################
    get "/blogs", to: "blogs#index", as: :blogs
    post "blogs", to: "blogs#create"
    get "/blogs/new", to: "blogs#new", as: :new_blog
    get "blogs/:title_for_slug/edit", to: "blogs#edit", as: "edit_blog"
    get "blogs/:title_for_slug", to: "blogs#show", as: "blog"
    patch "blogs/:title_for_slug", to: "blogs#update"
    put "blogs/:title_for_slug", to: "blogs#update"
    delete "blogs/:title_for_slug", to: "blogs#destroy"
  
  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end
  
  devise_for :users

  devise_scope :user do 
    get "/admin" => "devise/sessions#new" 
  end
  
  root 'home#index'

  match '/about'                      =>        'home#about',                       via: [:get],            :as => 'about'
  match '/college-consulting'         =>        'home#college_consulting',          via: [:get],            :as => 'college_consulting'
  match '/international-students'     =>        'home#international_students',      via: [:get],            :as => 'international_students'
  match '/additional-services'        =>        'home#additional_services',         via: [:get],            :as => 'additional_services'
  match "/contacts"                   =>        "contacts#create",                  via: [:post]
  match "/post_additional_services"   =>        "home#post_additional_services",    via: [:post],            :as => 'post_additional_services'
  match "/post_international_students"   =>        "home#post_international_students",    via: [:post],            :as => 'post_international_students'


  get '/sitemap.xml', to: redirect("https://s3.amazonaws.com/bleventhal/sitemaps/sitemap.xml.gz", status: 301)

end
